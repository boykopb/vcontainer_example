# vcontainer_example
- [ ] [VContainer link](https://vcontainer.hadashikick.jp/)

Simple example showing:
- register scene scope dependencies.
- register project scope dependencies.
- using factory for create instancies that uses scoped dependencies.