﻿using UnityEngine;
using VContainer.Unity;

namespace VContainer.Installers
{
    public class MainSceneInstaller : LifetimeScope
    {
        [SerializeField] private MainSceneScreen _mainSceneScreen;


        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<GamePresenter_Main>(Lifetime.Singleton);
            builder.RegisterComponent(_mainSceneScreen);

            builder.RegisterEntryPoint<GamePresenter_Main>();
        }
    }
}