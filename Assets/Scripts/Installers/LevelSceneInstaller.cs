using UnityEngine;
using VContainer.ScopeExamples;
using VContainer.Unity;

namespace VContainer.Installers
{
    public class LevelSceneInstaller : LifetimeScope
    {
        [SerializeField] private HelloScreen _helloScreen;
        [SerializeField] private Factory _factory;
    

        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<HelloWorldService>(Lifetime.Singleton);
            builder.Register<GamePresenter_Level>(Lifetime.Singleton);
            builder.Register<SceneScopedClass>(Lifetime.Singleton);
            builder.RegisterComponent(_helloScreen);
            builder.RegisterComponent(_factory);

            builder.Register<IObjectResolver, Container>(Lifetime.Singleton);
            builder.RegisterEntryPoint<GamePresenter_Level>();
        }
    }
}