﻿using VContainer.ScopeExamples;
using VContainer.Unity;

namespace VContainer.Installers
{
    public class ProjectLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<ProjectScopedClass>(Lifetime.Singleton);
        }
    }
}