﻿using VContainer.Unity;

namespace VContainer
{
    public class GamePresenter_Level : IStartable
    {
        readonly HelloWorldService _helloWorldService;
        private readonly HelloScreen _helloScreen;
        private readonly Factory _factory;
        private readonly ProjectScopedClass _projectScopedClass;

        public GamePresenter_Level(
            HelloWorldService helloWorldService,
            HelloScreen helloScreen, 
            Factory factory, 
            ProjectScopedClass projectScopedClass)
        {
            _helloWorldService = helloWorldService;
            _helloScreen = helloScreen;
            _factory = factory;
            _projectScopedClass = projectScopedClass;
        }


        void IStartable.Start()
        {
            _helloScreen.HelloButton.onClick.AddListener(() => _helloWorldService.HelloButton());
            _helloScreen.CreateButton.onClick.AddListener(() => _factory.Create());
            _helloScreen.ProjectLTSTest.onClick.AddListener(() => _projectScopedClass.Test());
        }
    }
}