﻿using UnityEngine.SceneManagement;
using VContainer.Unity;

namespace VContainer
{
    public class GamePresenter_Main : IStartable
    {
        private readonly MainSceneScreen _mainSceneScreen;
        private readonly ProjectScopedClass _projectScopedClass;

        public GamePresenter_Main(MainSceneScreen mainSceneScreen, ProjectScopedClass projectScopedClass)
        {
            _mainSceneScreen = mainSceneScreen;
            _projectScopedClass = projectScopedClass;
        }

        void IStartable.Start()
        {
            _mainSceneScreen.ProjectLTSTest.onClick.AddListener(() => _projectScopedClass.Test());
            _mainSceneScreen.LoadNextButton.onClick.AddListener(LoadNext);
        }

        private void LoadNext()
        {
            _mainSceneScreen.HideLoadNext();
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }

    }
}