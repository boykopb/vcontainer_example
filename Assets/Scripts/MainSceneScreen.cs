using UnityEngine;
using UnityEngine.UI;

namespace VContainer
{
    public class MainSceneScreen : MonoBehaviour
    {
        [SerializeField] private Button _projectLTSTest;
        [SerializeField] private Button _loadNextButton;

        public Button ProjectLTSTest => _projectLTSTest;

        public Button LoadNextButton => _loadNextButton;


        public void HideLoadNext()
        {
            _loadNextButton.gameObject.SetActive(false);
            _loadNextButton.onClick.RemoveAllListeners();
        }
    }
}
