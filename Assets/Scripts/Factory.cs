using UnityEngine;
using VContainer.Unity;

namespace VContainer
{
    public class Factory : MonoBehaviour
    {
        [SerializeField] private UITest _prefab;
        [SerializeField] private RectTransform _uiRoot;
    
        private IObjectResolver resolver;


        [Inject]
        public void Construct(IObjectResolver resolver)
        {
            this.resolver = resolver;
        }


        public void Create()
        {
            UITest instance = resolver.Instantiate(_prefab, _uiRoot);
        
            instance.Test();
        }
    }
}