﻿using UnityEngine;

namespace VContainer.ScopeExamples
{
    public class SceneScopedClass
    {
        private int count;
        public void Test()
        {
            Debug.Log($"This message is from SCENE scoped class: {++count}!");
        }
    }
}