﻿using UnityEngine;

namespace VContainer
{
    public class ProjectScopedClass
    {
        public void Test()
        {
            Debug.Log("This message is from PROJECT scoped class");
        }
    }
}