﻿using UnityEngine;
using VContainer.ScopeExamples;

namespace VContainer
{
    public class UITest : MonoBehaviour
    {
        private SceneScopedClass test;


        [Inject]
        public void Construct(SceneScopedClass test)
        {
            this.test = test;
        }


        public void Test()
        {
            test.Test();
        }
    }
}