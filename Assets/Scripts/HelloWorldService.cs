﻿namespace VContainer
{
    public class HelloWorldService
    {
        private int count;
        private int count1;


        public void Hello()
        {
            UnityEngine.Debug.Log($"Hello world. Count : {++count}");
        }
        
        
        public void HelloButton()
        {
            UnityEngine.Debug.Log($"Hello HelloButton. Count : {++count1}");
        }
    }
}